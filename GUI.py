from tkinter import *
from tkinter import ttk

import arrow as arrow

window = Tk()  # root widget

# GUI size
window.geometry("752x535")
window.minsize(600, 400)

# GUI title
window.title("Hand Gesture based Natural User Interface")

# GUI Icon
# Will enable later on.
icon = PhotoImage(file='logo.png')
window.iconphoto(True, icon)


# window.iconbitmap('D:\Python projects\Tkinter-trial\logo.ico')


def okButton():
    myLabel = Label(window,
                    text="Look I clicked a button!")
    myLabel.pack()


# def cancelButton():


def applyButton():
    myLabel = Label(window,
                    text="The selected dominant hand is " + clicked.get().lower() + " hand.")
    myLabel.pack()


def selected(event):
    myLabel = Label(window, text=myCombo.get()).pack()


options = ["Right", "Left"]

# Label
title_text_label = Label(window,
                         text="Hand Gesture based Natural User Interface",
                         font=('Helvetica',
                               20,
                               'bold'
                               )
                         )
title_text_label.pack()

dominant_hand_label = Label(window,
                            text="Dominant Hand",
                            font=('Helvetica',
                                  10
                                  )
                            )
dominant_hand_label.pack()

clicked = StringVar()
clicked.set(options[0])
# drop = OptionMenu(window, clicked, *options, command=selected)
# drop.pack(pady=20)
drop = OptionMenu(window, clicked, *options)
drop.pack(pady=20)
# drop.pack(padx=5,pady=5,sticky=W+E)
# drop.config(indictoron=0,compound='right',image=arrow,width=140)
# drop.image = arrow

myCombo = ttk.Combobox(window, value=options)
myCombo.current(0)
myCombo.bind("<<ComboboxSelected>>", selected)
myCombo.pack(pady=20)

# age_label = Label(window, text="Age")
# age_label.grid(row=1, column=0)


OK_button = Button(window,
                   text="OK",
                   padx=35,
                   command=okButton
                   )
OK_button.pack()

CANCEL_button = Button(window,
                       text="Cancel",
                       padx=25,
                       command=window.quit
                       )
CANCEL_button.pack()

APPLY_button = Button(window,
                      text="Apply",
                      padx=27,
                      command=applyButton
                      )
APPLY_button.pack()

window.mainloop()
