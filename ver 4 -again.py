from tkinter import *

window = Tk()   # root widget

# Label
text_label = Label(window,
                   text="Hello World!",
                   font=('Times New Roman',
                         20,
                         'bold'),
                   fg='#00FF00',
                   bg='black',
                   relief=SUNKEN,
                   bd=10)

# displaying it
text_label.pack()

window.mainloop()