from tkinter import *

window = Tk()  # instantiates an instance of window <<window->reference object for an object of class Tk()>>
window.geometry("420x420")
window.title("GUI App ver 2")

icon = PhotoImage(file='logo.png')
window.iconphoto(True,icon)

window.mainloop()  # places window on computer screen. Also, listens for events.

