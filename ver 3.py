from tkinter import *

window = Tk()
window.title("GUI App ver 3")


# Label
name_label = Label(window, text="Name")
name_label.grid(row=0, column=0)

age_label = Label(window, text="Age")
age_label.grid(row=1, column=0)


# Text Input
u_name = Entry(window, width=50, borderwidth=2)
#u_name.insert(0,"username")
u_name.grid(row=0, column=1)

u_age = Entry(window, width=50, borderwidth=2)
u_age.grid(row=1, column=1)


def on_click():
    print(f"User name is {u_name.get()}, and age is {u_age.get()}.")
    display_text = f"User name is {u_name.get()}, and age is {u_age.get()}."
    myLabel = Label(window,
                    text=display_text)
    myLabel.grid(row=3,column=1)


#Button
submit=Button(window,text="Submit", command=on_click)  #dont use parenthesis for function call -> call back function
submit.grid(row=2,column=1)

window.mainloop()
